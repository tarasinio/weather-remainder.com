<?php

namespace Tarasinio\WeatherRemainderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Tarasinio\API\Weather\WorldWeatherOnline\Client;

class DefaultController extends Controller
{
    public $cityFinderForm;
    
    
    public function indexAction()
    { 
            $this->cityFinderForm = $this->createForm(new \Tarasinio\WeatherRemainderBundle\Form\CityFinderType);
     
        $weatherManager = new Client();
        $weatherManager->setCity('$city');
        $weatherResult = $weatherManager->getResponse();
        return $this->render('TarasinioWeatherRemainderBundle:Default:index.html.twig', array(
            'cityFinderForm' => $this->cityFinderForm->createView()
        ));
    }
    
    public function reciveAction()
    {
        
        return $this->render('TarasinioWeatherRemainderBundle:Default:index.html.twig', array(
            'response' => $weatherResult,
            'cityFinderForm' => $this->cityFinderForm->createView()
                ));
    }
}